#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
void main(){
 char *file ="./txt.txt";
 char *file_cp ="/tmp/txt_cp.txt";
 ssize_t num_read;
 char buffer[4096];

	int fd = open(file, O_RDONLY);
	int fd_cp = open( file_cp, O_WRONLY | O_CREAT, 0666);

	if (fd <0 || fd_cp <0){
		write(0,"Error en abrir los fichero",30);
	}

   while(num_read = read(fd, buffer, sizeof buffer), num_read > 0) {
	write(fd_cp, buffer,num_read);
   }

  close(fd);
 close(fd_cp);
}
